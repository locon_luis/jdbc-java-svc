package com.kony.exegol.dto;

public class SystemUser {
  
  private String id;
  
  private String status;
  
  private String username;
  
  private String hashedPassword;
  
  private String email;
  
  private String firstName;
  
  private String lastName;
  
  private String middleName;
  
  private String createdby;
  
  private String modifiedby;
  
  private boolean userExist;
  
  private int errCode;
  
  private String errMessage;
  
  private boolean hasError;
  
  public SystemUser() {};

  public SystemUser(String id, String username, String status,  String hashedPassword, String email, String firstName, String lastName, String middleName, String createdby, String modifiedby, boolean userExist, int errCode, String errMessage, boolean hasError) {
    this.id = id;
    this.username = username;
    this.status = status;
    this.hashedPassword = hashedPassword;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.middleName = middleName;
    this.createdby = createdby;
    this.modifiedby = modifiedby;
    this.userExist = userExist;
    this.errCode = errCode;
    this.errMessage = errMessage;
    this.hasError = hasError;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }

  public String getHashedPassword() {
    return this.hashedPassword;
  }
  
  public void setHashedPassword(String hashedPassword) {
    this.hashedPassword = hashedPassword;
  }

  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return this.firstName;
  }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getMiddleName() {
    return this.middleName;
  }
  
  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getCreatedBy() {
    return this.createdby;
  }
  
  public void setCreatedBy(String createdby) {
    this.createdby = createdby;
  }

  public String getModifiedBy() {
    return this.modifiedby;
  }
  
  public void setModifiedBy(String modifiedby) {
    this.modifiedby = modifiedby;
  }
  
  public boolean getUserExist() {
    return this.userExist;
  }
	  
  public void setUserExist(boolean userExist) {
    this.userExist = userExist;
  }

  public int getErrCode() {
    return this.errCode;
  }
	  
  public void setErrCode(int errCode) {
    this.errCode = errCode;
  }
  
  public String getErrMessage() {
    return this.errMessage;
  }
	  
  public void setErrMessage(String errMessage) {
    this.errMessage = errMessage;
  }
  
  public boolean getHasError() {
    return this.hasError;
  }
	  
  public void setHasError(boolean hasError) {
    this.hasError = hasError;
  }
}