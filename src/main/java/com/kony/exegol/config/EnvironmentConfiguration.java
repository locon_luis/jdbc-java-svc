package com.kony.exegol.config;

import com.kony.adminconsole.commons.handler.EnvironmentConfigurationsHandler;
// import com.kony.exegol.handler.EnvironmentConfigurationsHandler;
import com.konylabs.middleware.controller.DataControllerRequest;
import java.io.Serializable;

public enum EnvironmentConfiguration implements Serializable {
	DBX_DB_HOST_URL, DBX_DB_PASSWORD, DBX_DB_USERNAME, QUEUEMASTER_DATABASE_DRIVER;
  
  public String getValue(DataControllerRequest requestInstance) {
    return EnvironmentConfigurationsHandler.getValue(name(), requestInstance);
  }
}
