package com.kony.exegol.config;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;

import com.konylabs.middleware.controller.DataControllerRequest;



public class ConnectionDB {
	private Connection cn= null;
	private static final Logger LOG = Logger.getLogger(ConnectionDB.class);
    // private ResultSet resultSet = null;
	
	public Connection getConnection(DataControllerRequest requestInstance) throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			String URI = getDatabaseURI(requestInstance);
			LOG.debug("@@@@@@@ URI DATABASE: " + URI);
			String USERNAME = getDatabaseUSERNAME(requestInstance);
			LOG.debug("@@@@@@@ URI USERNAME: " + USERNAME);
			String PASSWORD = getDatabasePASSWORD(requestInstance);
			LOG.debug("@@@@@@@ URI PWD: " + PASSWORD);
			
			cn = DriverManager.getConnection(URI, USERNAME, PASSWORD);
			return cn;
			
		} catch (Exception e) {
			System.out.print("e: " + e);
			LOG.error("ERROR IN GET CONNECTION: " + e.getMessage());
			throw e;
		}
	}
	
	/*private void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			
			if (cn != null) {
				cn.close();
			}
			
		} catch (Exception e) {
			System.out.println("ERROR is: " + e.getMessage());
		}
	}*/
	
	private String getDatabaseURI(DataControllerRequest requestInstance) {
		String URI = EnvironmentConfiguration.DBX_DB_HOST_URL.getValue(requestInstance);
		LOG.debug("@@@@@@@ URI ENV: " + URI);
		return URI;
	}
	
	private String getDatabaseUSERNAME(DataControllerRequest requestInstance) {
		String USERNAME = EnvironmentConfiguration.DBX_DB_USERNAME.getValue(requestInstance);
		LOG.debug("@@@@@@@ USERNAME ENV: " + USERNAME);
		return USERNAME;
	}
	
	private String getDatabasePASSWORD(DataControllerRequest requestInstance) {
		String PASSWORD = EnvironmentConfiguration.DBX_DB_PASSWORD.getValue(requestInstance);
		LOG.debug("@@@@@@@ PWD ENV: " + PASSWORD);
		return PASSWORD;
	}
}
