package com.kony.exegol.service;

import org.apache.commons.lang3.StringUtils;

import com.kony.exegol.dto.SystemUser;
import com.kony.exegol.exception.ApplicationException;
import com.kony.exegol.handler.SystemUserHandler;
import com.konylabs.middleware.controller.DataControllerRequest;
import org.apache.log4j.Logger;

public class ValidateUser {
	private static final Logger LOG = Logger.getLogger(ValidateUser.class);
	@SuppressWarnings("unused")
	public static String authenticateUser(String username, String password, DataControllerRequest requestInstance) throws Exception{
		String success = "Internal user was not found/created";
		try {
			// If comes data empty
			if (StringUtils.isAnyBlank(new CharSequence[] { username, password })) {
			    return "Data comes empty";
			} 
			// Read Internal User
			SystemUser internalUserProfile = SystemUserHandler.readInternalUser(username, password, requestInstance);
			success = "Internal User Found it!";
			LOG.debug("@@@@@@@@@@@ internalUserProfile: " + internalUserProfile.toString());
			LOG.debug("@@@@@@@@@@@ requestInstance: " + requestInstance.toString());
			if (!internalUserProfile.getUserExist()) {
				success = "";
				// Internal User is not found, I want to create the internal user
				boolean successCreated = SystemUserHandler.createInternalUser(username, password, requestInstance);
				success = (successCreated == true ? "Internal user was created" :  "Internal user failed to create");
				return success;
			}
			
			if (internalUserProfile.getUserExist() && internalUserProfile.getHasError()) {
				return internalUserProfile.getErrMessage();
			}
			
			return success;
		} catch (Exception e) {
			LOG.error("@@@@@@@@@@@ Exception on ValidateUser: " + e.getMessage());
			throw e;
			
		}
		
		
	}
	
}
