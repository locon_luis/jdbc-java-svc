package com.kony.exegol.service;



import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Result;
import org.apache.log4j.Logger;

public class ValidateUserService implements JavaService2{
private static final Logger LOG = Logger.getLogger(ValidateUserService.class);

  public Object invoke(String methodID, Object[] inputArray, DataControllerRequest requestInstance, DataControllerResponse responseInstance) throws Exception {
    try {
    	String username = requestInstance.getParameter("inputUsername");
        String password = requestInstance.getParameter("inputPassword");
        
        // Lets Begin
        String messageFromValidate = ValidateUser.authenticateUser(username, password, requestInstance);
        LOG.debug("@@@@@@@@ messageFromValidate: " + messageFromValidate);

        // String newID = requestInstance.getParameter("newIDUSER");
        
        Result result = new Result();
        result.addParam(new Param("inputUsername", username, "string"));
        result.addParam(new Param("inputPassword", password, "string"));
        result.addParam(new Param("messageFromValidate", messageFromValidate, "string"));
        return result;
      
    } catch (Exception e) {
    	Result errorResult = new Result();
        errorResult.addParam(new Param("errorMessage", "Error de la excepcion: " + e.getLocalizedMessage(), "string"));
        errorResult.addParam(new Param("errorMessageText", "Error de la excepcion2: " + e.getMessage(), "string"));
        LOG.debug("ERROR EXCEPTION e: " + e.getMessage());
        errorResult.addParam(new Param("errorCode", Integer.toString(505), "int"));
        return errorResult;
    }
  }
}