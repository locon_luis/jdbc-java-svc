package com.kony.exegol.exception;



public class ApplicationException extends Exception {
  private static final long serialVersionUID = 7065601577555260335L;
  
  private String errorMessage;
  
  public ApplicationException(String errorMessage) {
    this.errorMessage = errorMessage;
  }
  
  public ApplicationException(String errorMessage, Throwable cause) {
    super(cause);
    this.errorMessage = errorMessage;
  }
  
  public String getErrorMessage() {
    return this.errorMessage;
  }
  
}
