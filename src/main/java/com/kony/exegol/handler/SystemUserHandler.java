package com.kony.exegol.handler;

import com.kony.exegol.dto.SystemUser;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.kony.adminconsole.commons.utils.CommonUtilities;
import com.kony.adminconsole.commons.crypto.BCrypt;
import com.kony.exegol.utilities.ValidUserResult;
import com.kony.exegol.config.ConnectionDB;
import java.sql.Connection;
import java.sql.Statement;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SystemUserHandler {
	
	private static final Logger LOG = Logger.getLogger(SystemUserHandler.class);
	
	public static SystemUser readInternalUser(String username, String password, DataControllerRequest requestInstance) throws Exception {
		try {
			Connection cn = new ConnectionDB().getConnection(requestInstance);
			Statement statement = cn.createStatement();
	        // Result set get the result of the SQL query
			ResultSet resultSet = statement.executeQuery("select * from dbxdb.systemuser where Username = '"+ username +"'");
			LOG.debug("@@@@@@@@@@@ resultSet: " + resultSet.toString());
			// Map all the resultSet to SystemUser Structure
			SystemUser syus = mapResultToSystemUser(resultSet);
			if (syus.getUserExist()) {
				boolean pwdIsCorrect = isCorrectPassword(password, syus.getHashedPassword());
				if (pwdIsCorrect) {
					syus.setErrCode(0);
					syus.setErrMessage("All Legit!");
					syus.setHasError(false);
				} else {
					syus.setErrCode(ValidUserResult.INVALID_CREDENTIALS.getCode());
					syus.setErrMessage(ValidUserResult.INVALID_CREDENTIALS.getMessage());
					syus.setHasError(true);
				}
			}
			LOG.debug("@@@@@@@@@@@ syus1: " + syus != null ? syus.toString() : null);
			// return SystemUser
			cn.close();
			return syus;
		} catch (Exception e) {
			LOG.error("Exception SystemUserHandler: " + e);
			throw e;
		}
	}
	
	public static boolean createInternalUser(String username, String password, DataControllerRequest requestInstance) throws Exception{
		boolean wasCreated = false;
		try {
			// Create an id
		    String id = CommonUtilities.getNewId().toString();
		    String newPassword_Salt = BCrypt.gensalt(11);
	        String newPassword_hashed = BCrypt.hashpw(password, newPassword_Salt);
		    LOG.debug("@@@@@@@@@@@ new ID: " + id.toString());
		    LOG.debug("@@@@@@@@@@@ new hashedPWD: " + newPassword_hashed);
			Connection cn = new ConnectionDB().getConnection(requestInstance);
			PreparedStatement ps = cn.prepareStatement("insert into dbxdb.systemuser (id, Status_id, Username, Password, Email, FirstName, LastName, MiddleName, createdby, modifiedby) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, id);
			ps.setString(2, "SID_ACTIVE");
			ps.setString(3, username);
			ps.setString(4, newPassword_hashed);
			ps.setString(5, username + "@banpro.ni");
			ps.setString(6, requestInstance.getParameter("firstName") == null ? username : requestInstance.getParameter("firstName"));
			ps.setString(7, requestInstance.getParameter("lastName") == null ? username : requestInstance.getParameter("lastName"));
			ps.setString(8, "");
			ps.setString(9, "konydev-AD");
			ps.setString(10, "konydev-AD");
			ps.executeUpdate();
			requestInstance.setAttribute("newIDUSER", id);
			assignRoleDefault(id, requestInstance);
			wasCreated = true;
			cn.close();
			return wasCreated;
		} catch (Exception e) {
			LOG.error("@@@@@@@@@@@ Excepcion dentro de SystemUserHandler: " + e.getMessage());
			throw e;
		}
	}
	
	private static void assignRoleDefault(String newInternalUserID, DataControllerRequest requestInstance) throws Exception {
		try {
			
			Connection cn = new ConnectionDB().getConnection(requestInstance);
			PreparedStatement ps = cn.prepareStatement("INSERT INTO dbxdb.userrole (User_id, Role_id, hasSuperAdminPrivilages, createdby, modifiedby, softdeleteflag) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setString(1, newInternalUserID);
			ps.setString(2, "RID_AD_EMPLOYEE");
			ps.setInt(3, 0);
			ps.setString(4, "konydev-AD");
			ps.setString(5, "konydev-AD");
			ps.setInt(6, 0);
			ps.executeUpdate();
			cn.close();
		} catch (Exception e) {
			LOG.error("ERROR CREATING USER ROLE: " + e);
			throw e;
		}
	}
	
 	private static SystemUser mapResultToSystemUser(ResultSet resultSet) throws SQLException {
		SystemUser sysus = new SystemUser();
		if (!resultSet.next()) {
			LOG.debug("@@@@@@@@@@@@ resultSet.next(): " + resultSet.next());
			sysus.setUserExist(false);
			return sysus;
		} else {
			do {
				sysus.setId(resultSet.getString("id")); 
				sysus.setUsername(resultSet.getString("Username"));
				sysus.setStatus(resultSet.getString("Status_id"));
				sysus.setHashedPassword(resultSet.getString("Password"));
				sysus.setEmail(resultSet.getString("Email"));
				sysus.setFirstName(resultSet.getString("FirstName"));
				sysus.setLastName(resultSet.getString("LastName"));
				sysus.setMiddleName(resultSet.getString("MiddleName"));
				sysus.setCreatedBy(resultSet.getString("createdby"));
				sysus.setModifiedBy(resultSet.getString("modifiedby"));
				sysus.setUserExist(true);
			} while(resultSet.next());
		}
		
		LOG.debug("@@@@@@@@@@@@ sysus2: " + sysus.toString());
		return sysus;
	}
	
	public static boolean isCorrectPassword(String plainTextPassword, String hashedPassword) {
	    try {
	      return BCrypt.checkpw(plainTextPassword, hashedPassword);
	    } catch (Exception e) {
	      return false;
	    } 
	}
}
